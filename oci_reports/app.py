import json
from tokenize import Name
import oci
import boto3
from botocore.exceptions import ClientError
from botocore.exceptions import NoCredentialsError
import os
from dotenv import load_dotenv
import requests
import traceback, json, configparser
# from aws_xray_sdk.core import patch_all
# patch_all()


def upload_file(file_name, bucket, object_name=None):
    """Upload a file to an S3 bucket

    :param file_name: File to upload
    :param bucket: Bucket to upload to
    :param object_name: S3 object name. If not specified then file_name is used
    :return: True if file was uploaded, else False
    """

    # If S3 object_name was not specified, use file_name
    if object_name is None:
        object_name = os.path.basename(file_name)

    load_dotenv()
    ACCESS_KEY = os.getenv('ACCESS_KEY')
    SECRET_KEY = os.getenv('SECRET_KEY')
    # Upload the file
    s3 = boto3.client('s3', aws_access_key_id=ACCESS_KEY,
                      aws_secret_access_key=SECRET_KEY)

    try:
        s3.upload_file(file_name, bucket, object_name)
        print("Upload Successful")
        return True
    except FileNotFoundError:
        print("The file was not found")
        return False
    except NoCredentialsError:
        print("Credentials not available")
        return False
    
def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    try:
                # Set up config
        #config = oci.config.from_file("~\.oci\config.txt","ADMIN_USER")
        # Create a service client
        #identity = oci.identity.IdentityClient(config)
        # Get the current user
        #user = identity.get_user(config["user"]).data
        #print(user)
        # Initialize boto3 client at global scope for connection reuse
        client = boto3.client('ssm')
        responseparam = client.get_parameter(Name='oci_configuration')
        print(responseparam)
        print("USer identity can be established1")
      
        #reporting_namespace = 'ax8hkeiizrq6'
        reporting_namespace = 'bling'
                                                    
        # Download all usage and cost files. You can comment out based on the specific need:
        prefix_file = ""                     #  For cost and usage files
        #prefix_file = "reports/cost-csv"   #  For cost
        #prefix_file = "reports/usage-csv"  #  For usage
        
        
        config_with_key_file = {
            "user": 'ocid1.user.oc1..aaaaaaaafl4jxczrzgy7tqvm4mmvap4qib3nraqxs2aaad3dmw45ulkexmha',
            "key_file": 'OCI_REPORTS/oci_reports/oci_api_key.pem',
            "fingerprint": '30:46:bc:2b:7d:37:7a:00:52:af:bc:1e:a2:84:59:32',
            "tenancy": 'ocid1.tenancy.oc1..aaaaaaaa33ijrbogdsgrvat44py342kyq7pza4yo22q2lkv5xil3a6sby5wq',
            "pass" :'phrase=mypass',
            "region": 'us-sanjose-1'
        }
        oci.config.validate_config(config_with_key_file)
        config=config_with_key_file
                        
        # config = oci.config.from_file("~\.oci\config.txt","ADMIN_USER")
        print("USer identity can be established2")
        
       
                        
        # Make a directory to receive reports
        # if not os.path.exists(destintation_path):
        #     os.mkdir(destintation_path)
        destintation_path = '/tmp/downloaded_reports'
        os.chdir('/tmp')
        if not os.path.exists(destintation_path):
            os.mkdir(destintation_path)  
            
        # tempdir = tempfile.mkdtemp(prefix="downloaded_reports-")
        # print(tempdir) # prints e.g. /tmp/myapplication-ztcy6s2w
        
        # Update these values
        
                        
        # Get the list of reports
        
        #reporting_bucket = "bucket1"
        reporting_bucket = config['tenancy']
        # object_storage = oci.object_storage.ObjectStorageClient(config)
        # report_bucket_objects = object_storage.list_objects(reporting_namespace, reporting_bucket, prefix=prefix_file)
                        
        # for o in report_bucket_objects.data.objects:
        #     print('Found file ' + o.name)
            
        #     object_details = object_storage.get_object(reporting_namespace, reporting_bucket, o.name)
        #     filename = o.name.rsplit('/', 1)[-1]
        #     print(filename)
        #     with open(destintation_path + '/' + filename, 'wb') as f:
        #         for chunk in object_details.data.raw.stream(1024 * 1024, decode_content=False):
        #             f.write(chunk)
            
        #     upload_file(destintation_path + '/' + filename,"oci-data-bucket",filename)

                        
            # print('----> File ' + o.name + ' Downloaded')

    except requests.RequestException as e:
        # Send some context about this error to Lambda Logs
        print(e)
        raise e

    return {
        "statusCode": 200,
        "body": json.dumps(
            {
                "message": "oci_reports"+responseparam['Parameter']['Value'],
                # "location": ip.text.replace("\n", "")
            }
        ),
    }
