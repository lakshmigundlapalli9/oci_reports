requests
wheel
oci==2.27.0
boto3
python-dotenv==0.20.0
aws_xray_sdk
